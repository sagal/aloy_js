import { parse } from "https://deno.land/x/xml@v1.0.2/mod.ts";
import { S3Bucket } from "https://deno.land/x/s3@0.4.1/mod.ts";

const clientId = Deno.env.get("CLIENT_ID");
const clientIntegrationId = Deno.env.get("CLIENT_INTEGRATION_ID");
const clientEcommerce = JSON.parse(Deno.env.get("CLIENT_ECOMMERCE"));

const endpoint = "http://201.217.138.146:7077/aintegrasagal.aspx"
const username = Deno.env.get("USERNAME")
const password = Deno.env.get("PASSWORD")
const awsKeyId = Deno.env.get("AWS_ACCESS_KEY_ID")
const awsSecretKey = Deno.env.get("AWS_SECRET_ACCESS_KEY")


const bucket = new S3Bucket({
    accessKeyID: awsKeyId,
    secretKey: awsSecretKey,
    bucket: "sagal-excel-uploads-0181239103940129",
    region: "us-east-1"
});


const uploadImages = async (sku, images) => {
    let future_images = []

    for (let i = 1; i <= images.length; i++) {
        let buffer = Buffer.from(images[i - 1].replace(/^data:image\/\w+;base64,/, ""), 'base64')
        let imageName = `${sku}_${i}`
        future_images.push(uploadImage(imageName, buffer))
    }

    let uploaded = await Promise.all(future_images)
    return uploaded
}

const uploadImage = async (imageName, buffer) => {
    await bucket.putObject(`/aloy_images/${imageName}.jpg`, buffer, {
        contentType: "image/jpeg",
        acl: 'public-read'
    })
    console.log(`Image uploaded: https://sagal-excel-uploads-0181239103940129.s3.amazonaws.com/aloy_images/${imageName}.jpg`)
    return `https://sagal-excel-uploads-0181239103940129.s3.amazonaws.com/aloy_images/${imageName}.jpg`
}

const fetchImages = async (sku) => {
    const soapRequest =
        `
            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:int="IntegraCRM">
                <soapenv:Header/>
                <soapenv:Body>
                <int:IntegraSAGAL.LISTADEIMAGENESDEARTICULOS>
                    <int:Usuario>${username}</int:Usuario>
                    <int:Password>${password}</int:Password>
                    <int:Codigoinicial>${sku}</int:Codigoinicial>
                    <int:Codigofinal>${sku}</int:Codigofinal>
                </int:IntegraSAGAL.LISTADEIMAGENESDEARTICULOS>
                </soapenv:Body>
            </soapenv:Envelope>
        `
    let imageResponse = await fetch(endpoint, {
        method: "POST",
        headers: {
            "Content-Type": "text/xml;charset=UTF-8",
        },
        body: soapRequest,
    });
    let response = await imageResponse.text();
    response = parse(response)
    response =
        response
        ["SOAP-ENV:Envelope"]
        ["SOAP-ENV:Body"]
        ["IntegraSAGAL.LISTADEIMAGENESDEARTICULOSResponse"]
        ["Imagenesdearticulos"]

    let images = []

    if (response) {
        images =
            response
            ["ImagenesDeArticulos.Articulo"]
            ["Imagenes"]

        Array.isArray(images["Imagen"]) ? (images = images["Imagen"]) : images = [images["Imagen"]]
    }
    return images
}

const fetchArticles = async () => {
    const soapRequest =
        `
        <soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:int='IntegraCRM'>
        <soapenv:Header/>
        <soapenv:Body>
            <int:IntegraSAGAL.LISTADEARTICULOSDEUNAMARCA>
                <int:Usuario>${username}</int:Usuario>
                <int:Password>${password}</int:Password>
                <int:Grucod>10</int:Grucod>
                <int:Marca>0</int:Marca>
                <int:Fechadesde>2000-01-01</int:Fechadesde>
                <int:Incluirimagenes>N</int:Incluirimagenes>
            </int:IntegraSAGAL.LISTADEARTICULOSDEUNAMARCA>
        </soapenv:Body>
        </soapenv:Envelope>
    `


    let productsAPIResponse = await fetch(endpoint, {
        method: "POST",
        headers: {
            "Content-Type": "text/xml;charset=UTF-8",
        },
        body: soapRequest,
    });
    let response = await productsAPIResponse.text();
    response = parse(response)
    response =
        response
        ["SOAP-ENV:Envelope"]
        ["SOAP-ENV:Body"]
        ["IntegraSAGAL.LISTADEARTICULOSDEUNAMARCAResponse"]
        ["Articulosdeunamarca"]
        ["ArticulosDeUnaMarca.Articulo"];

    return response;
}

async function processArticles(rawArticles) {
    for (let article of rawArticles) {
        try {
            let sku = article.Codigo
            let images = await fetchImages(sku);
            let uploadedImages = await uploadImages(sku, images);
            let payload = {
                sku: sku,
                client_id: clientId,
                integration_id: clientIntegrationId,
                options: {
                    merge: false,
                },
                ecommerce: await fetchEcommerce(uploadedImages, article)
            }

            await sagalDispatch(payload)
        } catch (e) {
            console.log(`Could not send article: ${e.message} - ${JSON.stringify(article)}`)
        }
    }
}



async function fetchEcommerce(uploadedImages, article) {
    let variants = []

    if (article.Colores) {
        if (!Array.isArray(article.Colores.Color)) {
            article.Colores.Color = [article.Colores.Color];
        }
        for (let x of article.Colores.Color) {
            let variantSku = x.Codigo;
            let variantImages = await fetchImages(variantSku);
            let variantUploadedImages = await uploadImages(variantSku, variantImages);
            variants.push({
                sku: variantSku,
                properties: [
                    { "images": variantUploadedImages },
                    {
                        "attributes": {
                            "COLOR": `${x.Nombre}`
                        }
                    }
                ]
            });
        }
    }

    return Object.values(clientEcommerce).map((ecommerce_id) => {
        let ecommerceValues = {
            ecommerce_id: ecommerce_id,
            properties: [
                { "images": uploadedImages },
                { "name": article.Nombre },
                { "description": article.Descripcion },
                {
                    "attributes": {
                        "Marca": article.Marca,
                        ...(article.CodigoEan && { "EAN": `${article.CodigoEan}` })
                    }
                }
            ],
            variants: variants
        };

        return ecommerceValues;
    });
}

const init = async () => {
    let parsedResponse = await fetchArticles();
    await processArticles(parsedResponse)
}

await init();