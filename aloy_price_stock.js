import { parse } from "https://deno.land/x/xml@v1.0.2/mod.ts";


const clientId = Deno.env.get("CLIENT_ID");
const clientIntegrationId = Deno.env.get("CLIENT_INTEGRATION_ID");
const clientEcommerce = JSON.parse(Deno.env.get("CLIENT_ECOMMERCE"));

const endpoint = "http://201.217.138.146:7077/aintegrasagal.aspx"
const username = Deno.env.get("USERNAME")
const password = Deno.env.get("PASSWORD")


const soapRequest =
    `
        <soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:int='IntegraCRM'>
        <soapenv:Header/>
        <soapenv:Body>
            <int:IntegraSAGAL.LISTADEARTICULOSDEUNAMARCA>
                <int:Usuario>${username}</int:Usuario>
                <int:Password>${password}</int:Password>
                <int:Grucod>10</int:Grucod>
                <int:Marca>0</int:Marca>
                <int:Fechadesde>2000-01-01</int:Fechadesde>
                <int:Incluirimagenes>N</int:Incluirimagenes>
            </int:IntegraSAGAL.LISTADEARTICULOSDEUNAMARCA>
        </soapenv:Body>
        </soapenv:Envelope>
        `

async function fetchArticles() {
    let productsAPIResponse = await fetch(endpoint, {
        method: "POST",
        headers: {
            "Content-Type": "text/xml;charset=UTF-8",
        },
        body: soapRequest,
    });
    let response = await productsAPIResponse.text();
    response = parse(response);

    response =
        response
        ["SOAP-ENV:Envelope"]
        ["SOAP-ENV:Body"]
        ["IntegraSAGAL.LISTADEARTICULOSDEUNAMARCAResponse"]
        ["Articulosdeunamarca"]
        ["ArticulosDeUnaMarca.Articulo"];
    return response;
}

async function processArticles(rawArticles) {
    for (let article of rawArticles) {
        try {
            let payload = {
                sku: article.Codigo,
                client_id: clientId,
                integration_id: clientIntegrationId,
                options: {
                    merge: false,
                },
                ecommerce: Object.values(clientEcommerce).map(ecommerce_id => {
                    let ecommerceValues = {
                        ecommerce_id: ecommerce_id,
                        options: {
                            override_create: {
                                "send": false
                            },
                            override_update: "default"
                        },
                        properties: [
                            { "stock": article.Stock },
                            {
                                "price": {
                                    "value": parseFloat(article.Precio),
                                    "currency": article.Moneda == "1" ? "$" : "U$S"

                                }
                            },
                        ],
                        variants: []
                    }

                    if (article.Colores) {

                        if (!Array.isArray(article.Colores.Color)) {
                            article.Colores.Color = [article.Colores.Color]
                        }
                        ecommerceValues.variants = article.Colores.Color.map((x) => {
                            return {
                                sku: x.Codigo,
                                properties: [
                                    { "stock": x.Stock },
                                ]
                            }
                        })

                    }
                    return ecommerceValues
                })
            }
            await sagalDispatch(payload)
        } catch (e) {
            console.log(`Could not create: ${e.message} - ${JSON.stringify(article)}`)
        }
    }

}

const init = async () => {
    let response = await fetchArticles();
    await processArticles(response)
}


await init();

